const imgUtils = require("../utils/Image_utils");
const chartist = require("../utils/make-chartist");

exports.histogramAnalysisFile = async (ImageFilename) =>{
    try{
        let bitmapImg = await imgUtils.LoadBitmapImage(ImageFilename);
        let difference = this.histogramAnalysis(bitmapImg.data);
        return difference;
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in visual_attack_RGB_File, the error is: "${error}"`);
    }
}

exports.histogramAnalysis = (ImgBytes) =>{
    let arrayCount = this.histogramCount(ImgBytes);
    //Calculate the diference between the pair byte count. 
    //Ej: count byte 0 = 452, count byte 1 = 354, difference: 98
    //if difference is nearby to Zero (0) then maybe its a hidden message
    let difference=0;
    for (let index = 1; index < arrayCount.length; index+=2) {
        difference += Math.abs(arrayCount[index] - arrayCount[index-1]);        
    }
    return difference;
}

exports.histogramCountFile = async (ImageFilename) =>{
    try{
        let bitmapImg = await imgUtils.LoadBitmapImage(ImageFilename);
        let arrayCount = this.histogramCount(bitmapImg.data);
        return arrayCount;
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in visual_attack_RGB_File, the error is: "${error}"`);
    }
}


exports.histogramCount = (ImgBytes) =>{
    let arrayCount = new Array(256).fill(0); //make a array of 255 elements to count and assign Zero to all array elements
    //loop the array to increase 1 by each byte value gotten.
    for (let index = 0; index < ImgBytes.length; index++) {
        const element = ImgBytes[index];
        arrayCount[element] += 1;
    }
    //return the array with the count of all pixels image
    return arrayCount;
}

exports.saveHistogram = async (HistogramCount, filename) => {
    let dataTest = {data: HistogramCount,
        labels: [...Array(HistogramCount.length).keys()],
        labels_title: "Cantidad",
        data_title: "Byte"
    };
   await chartist.makeBarChart(3000,1500, dataTest, filename);
}