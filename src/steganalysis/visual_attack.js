const imgUtils = require("../utils/Image_utils");

exports.visual_attack_RGB_File = async (strImageAttacked, strImageResult) => {
    try{
        let bitmapImg = await imgUtils.LoadBitmapImage(strImageAttacked);
        let bytesIMGResult = this.visual_attack_RGB(bitmapImg.data);
        await imgUtils.writeBytesImage(bytesIMGResult, strImageResult, 
                                        bitmapImg.height, bitmapImg.width, {rgba: true});
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in visual_attack_RGB_File, the error is: "${error}"`);
    }

}
/*
    @param ImgBytes: buffer
    @returns a new bitmap with this condition
    IF LSB byte is 1, change byte pixel to 255 else change byte pixel to 0
*/
exports.visual_attack_RGB = (ImgBytes) => {
/* 
//the pixel's byte is assign in this way: 
let red = ImgBytes[idx + 0];
let green = ImgBytes[idx + 1];
let blue = ImgBytes[idx + 2];
let alpha = ImgBytes[idx + 3];
*/
    for (let indByte = 0; indByte < ImgBytes.length; indByte+=4) {
        let r = 0, g = 0, b = 0;
        if (ImgBytes[indByte] % 2 == 1) 
            r = 255;
        if (ImgBytes[indByte+1] % 2 == 1) 
            g = 255; 
        if (ImgBytes[indByte+2] % 2 == 1) 
            b = 255;
        ImgBytes[indByte] = r;
        ImgBytes[indByte+1] = g;
        ImgBytes[indByte+2] = b;
    }
    return ImgBytes;
}