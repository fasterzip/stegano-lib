/** 
    This source code is used to apply LSB MATCHING on an image. 
    However the function encodeLSB and decodeLSB can be a support 
    to apply in sound or other objects containers that can be applied LSB MATCHING.

    ----------------------------------------------------------------------------------

    Este código fuente sirve para aplicar LSB MATCHING sobre una imágen. 
    Sin embargo la función encodeLSB y decodeLSB pueden ser un soporte 
    para aplicar en sonido u otros objetos contenedores que se pueda aplicar 
    LSB MATCHING. 

**/
const jimp_lib = require('jimp');
const fs = require('fs');
const bits_lib = require('../utils/bit_manipulation');
const lsb_replacement_lib = require("../methods_stegano/LSB_Replacement_Image");

//Enums
const ChannelImage = {red: 0, green: 1, blue: 2, alpha: 3, RGB: 4};
exports.ChannelImage = ChannelImage;

//End enums

exports.encodeImg = async (strImgFile, strMessageFile, strEncodedFile, ChannelHide) => {
    try{
        let obj_jimp = await jimp_lib.read(strImgFile); //read the Image file in a JIMP object
        let bytesImg = obj_jimp.bitmap.data; //get the bytes to contain hide

        let msgBytes = fs.readFileSync(strMessageFile); //TODO: Check if the best way to do
        
        let lengthBuffer = bits_lib.Int32toBytes(msgBytes.length); //return Buffer with 4 bytes length
        msgBytes = Buffer.concat([lengthBuffer, msgBytes]);
        
        let bytesImgEncoded = this.encodeLSBMatching(bytesImg, msgBytes, ChannelHide);
        obj_jimp.bitmap.data = bytesImgEncoded;
        await obj_jimp.deflateStrategy(0)
        .filterType(jimp_lib.PNG_FILTER_NONE)
        .writeAsync(strEncodedFile);
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in encodeImg, the error is: "${error}"`);
    }
    }

exports.encodeLSBMatching = (ImgBytes, MsgBytesHide, ChannelHide) => {
    let encodedIMGBytes = Buffer.alloc(ImgBytes.length);
    //validation enum type ChannelHide and calculate maximum size of the container image
    let maxBitSizeContainer;
    let hideByte = ChannelHide; //hide on the selected channel byte (R, G, B or Alpha)
    switch (ChannelHide){
        case ChannelImage.red  : case ChannelImage.green: 
        case ChannelImage.blue : case ChannelHide.alpha:
            maxBitSizeContainer = (ImgBytes.length / 4) / 8;
            break;
        case ChannelImage.RGB:
            maxBitSizeContainer = ((ImgBytes.length / 4) * 3) / 8; //doesnt hide on alpha bytes
            break;        
        default:
            throw new Error("ChannelHide must be ChannelImage.red, ChannelImage.blue, ChannelImage.green, ChannelImage.alpha or ChannelImage.RGB");
    }

    if (maxBitSizeContainer < MsgBytesHide.length){
        throw new Error(`Maximum size supported by the container image is ${maxBitSizeContainer} and is less than the mesage to hide (${MsgBytesHide.length})`);
    }

    //transforms byte message buffer to bit buffer
    let bitsBufferMsg = bits_lib.convertBufferBytetoBufferBit(MsgBytesHide);

    /*
    //the pixel's byte is assign in this way: 
    let red = ImgBytes[idx + 0];
    let green = ImgBytes[idx + 1];
    let blue = ImgBytes[idx + 2];
    let alpha = ImgBytes[idx + 3];
    */

    if (ChannelHide == ChannelImage.RGB) {        
        //hide rgb
        let indBit=0;
        for (let indByte = 0; indByte < ImgBytes.length; indByte++) {
            const element = ImgBytes[indByte];
            encodedIMGBytes[indByte] = element;
            if (indByte % 4 !== ChannelImage.alpha){ //alpha channel omited
                //check if the bit to hide isn't the same that the channel has
                //if is the same, just ignore it
                if (bitsBufferMsg[indBit++] !== (element & 1)){
                    //change it to hide each bitsBufferMsg element
                    let arrAdd = [-1, 1];
                    //random choose add 1 or substract 1
                    let add = arrAdd[Math.floor(Math.random() * 2)];
                    add = element == 255 ? -1:  1; //if element = 255, just substract 1
                    add = element ==  0  ?  1: -1; //if element = 0, just add 1
                    encodedIMGBytes[indByte] = element + add;
                }

            }
        }        
    }else{
        //hide in 1 channel
        let indBit=0;
        for (let indByte = 0; indByte < ImgBytes.length; indByte++) {
            const element = ImgBytes[indByte];
            encodedIMGBytes[indByte] = element;
            if (indByte % 4 == hideByte){
                //check if the bit to hide isn't the same that the channel has
                //if is the same, just ignore it
                if (bitsBufferMsg[indBit++] !== (element & 1)){
                    //change it to hide each bitsBufferMsg element
                    let arrAdd = [-1, 1];
                    //random choose add 1 or substract 1
                    let add = arrAdd[Math.floor(Math.random() * 2)];
                    add = element == 255 ? -1:  1; //if element = 255, just substract 1
                    add = element ==  0  ?  1: -1; //if element = 0, just add 1
                    encodedIMGBytes[indByte] = element + add;
                }
            }
        }
    }
    return encodedIMGBytes;
}



exports.decodeImg = async (strImgFile, strMsgFileHidden, ChannelHide) => {
    lsb_replacement_lib.decodeImg(strImgFile, strMsgFileHidden,ChannelHide);
}

exports.decodeLSBMatching = (ImgBytes, ChannelHide, lengthMsg, offsetBitsHidden = 0) => {    
    return lsb_replacement_lib.decodeLSB(ImgBytes, ChannelHide, length, offsetBitsHidden);
}
