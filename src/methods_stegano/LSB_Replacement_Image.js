/** 
    This source code is used to apply LSB REPLACEMENT on an image. 
    However the function encodeLSB and decodeLSB can be a support 
    to apply in sound or other objects containers that can be applied LSB REPLACEMENT.

    ----------------------------------------------------------------------------------

    Este código fuente sirve para aplicar LSB REPLACEMENT sobre una imágen. 
    Sin embargo la función encodeLSB y decodeLSB pueden ser un soporte 
    para aplicar en sonido u otros objetos contenedores que se pueda aplicar 
    LSB REPLACEMENT. 

**/
const jimp_lib = require('jimp');
const fs = require('fs');
const bits_lib = require('../utils/bit_manipulation');

//Enums
const ChannelImage = {red: 0, green: 1, blue: 2, alpha: 3, RGB: 4};
exports.ChannelImage = ChannelImage;

//End enums

exports.encodeImg = async (strImgFile, strMessageFile, strEncodedFile, ChannelHide) => {
    try{
        let obj_jimp = await jimp_lib.read(strImgFile); //read the Image file in a JIMP object
        let bytesImg = obj_jimp.bitmap.data; //get the bytes to contain hide

        let msgBytes = fs.readFileSync(strMessageFile); //TODO: Check if the best way to do
        
        let lengthBuffer = bits_lib.Int32toBytes(msgBytes.length); //return Buffer with 4 bytes length
        msgBytes = Buffer.concat([lengthBuffer, msgBytes]);
        
        let bytesImgEncoded = this.encodeLSB(bytesImg, msgBytes, ChannelHide);
        obj_jimp.bitmap.data = bytesImgEncoded;
        await obj_jimp.deflateStrategy(0)
        .filterType(jimp_lib.PNG_FILTER_NONE)
        .writeAsync(strEncodedFile);
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in encodeImg, the error is: "${error}"`);
    }
    }

exports.encodeLSB = (ImgBytes, MsgBytesHide, ChannelHide) => {
    let encodedIMGBytes = Buffer.alloc(ImgBytes.length);
    //validation enum type ChannelHide and calculate maximum size of the container image
    let maxBitSizeContainer;
    let hideByte = ChannelHide; //hide on the selected channel byte (R, G, B or Alpha)
    switch (ChannelHide){
        case ChannelImage.red  : case ChannelImage.green: 
        case ChannelImage.blue : case ChannelHide.alpha:
            maxBitSizeContainer = (ImgBytes.length / 4) / 8;
            break;
        case ChannelImage.RGB:
            maxBitSizeContainer = ((ImgBytes.length / 4) * 3) / 8; //doesnt hide on alpha bytes
            break;        
        default:
            throw new Error("ChannelHide must be ChannelImage.red, ChannelImage.blue, ChannelImage.green, ChannelImage.alpha or ChannelImage.RGB");
    }

    if (maxBitSizeContainer < MsgBytesHide.length){
        throw new Error(`Maximum size supported by the container image is ${maxBitSizeContainer} and is less than the mesage to hide (${MsgBytesHide.length})`);
    }

    //transforms byte message buffer to bit buffer
    let bitsBufferMsg = bits_lib.convertBufferBytetoBufferBit(MsgBytesHide);

    /*
    //the pixel's byte is assign in this way: 
    let red = ImgBytes[idx + 0];
    let green = ImgBytes[idx + 1];
    let blue = ImgBytes[idx + 2];
    let alpha = ImgBytes[idx + 3];
    */

    if (ChannelHide == ChannelImage.RGB) {        
        //hide rgb
        let indBit=0;
        for (let indByte = 0; indByte < ImgBytes.length; indByte++) {
            const element = ImgBytes[indByte];
            encodedIMGBytes[indByte] = element;
            if (indByte % 4 !== ChannelImage.alpha){ //alpha channel omited
                //change it to hide each bitsBufferMsg element 
                encodedIMGBytes[indByte] = (element & ~1) | bitsBufferMsg[indBit++];
            }
        }        
    }else{
        //hide in 1 channel
        let indBit=0;
        for (let indByte = 0; indByte < ImgBytes.length; indByte++) {
            const element = ImgBytes[indByte];
            encodedIMGBytes[indByte] = element;
            if (indByte % 4 == hideByte){
                //change it to hide each bitsBufferMsg element 
                encodedIMGBytes[indByte] = (element & ~1) | bitsBufferMsg[indBit++];
            }
        }
    }
    return encodedIMGBytes;
}



exports.decodeImg = async (strImgFile, strMsgFileHidden, ChannelHide) => {
    try{
        let obj_jimp = await jimp_lib.read(strImgFile); //read the Image file in a JIMP object
        let bytesImg = obj_jimp.bitmap.data; //get the bytes to contain hide
        
        const Int32Len = 32; //4 fixed bytes * 8 bits
        //get the length of the buffer size hidden
        let BufferDecodedLength = this.decodeLSB(bytesImg, ChannelHide, Int32Len);
        let lengthDecoded = bits_lib.BytestoInt32(bits_lib.convertBufferBitstoBufferBytes(BufferDecodedLength));
        lengthDecoded = lengthDecoded * 8; //convert bytes units to bits
        let bitsMsgDecoded = this.decodeLSB(bytesImg, ChannelHide, lengthDecoded, Int32Len);
        let bytesMsgDecoded = bits_lib.convertBufferBitstoBufferBytes(bitsMsgDecoded);
        fs.writeFileSync(strMsgFileHidden, bytesMsgDecoded);
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in encodeImg, the error is: "${error}"`);
    }
}

exports.decodeLSB = (ImgBytes, ChannelHide, lengthMsg, offsetBitsHidden = 0) => {
    let decodedMsgBytes = Buffer.alloc(lengthMsg);

    //validate length
    if (!lengthMsg || lengthMsg<=0){
        throw new Error("Length must be larger than zero");
    }

    //validate offset
    if(offsetBitsHidden >= lengthMsg || offsetBitsHidden < 0){
        throw new Error("Offset must be between zero and length - 1");        
    }

    //validation enum type ChannelHide
    let hideByte = ChannelHide; //hide on the selected channel byte (R, G, B or Alpha)
    switch (ChannelHide){
        case ChannelImage.red  : case ChannelImage.green: 
        case ChannelImage.blue : case ChannelHide.alpha:
            break;
        case ChannelImage.RGB:
            break;        
        default:
            throw new Error("ChannelHide must be ChannelImage.red, ChannelImage.blue, ChannelImage.green, ChannelImage.alpha or ChannelImage.RGB");
    }

    if (ChannelHide == ChannelImage.RGB) {        
        //show decoded in rgb
        let indBit=0;
        let contByte =  0;
        let bitsDecoded = 0;
        while (indBit < lengthMsg) {
            if (contByte % 4 !== this.ChannelImage.alpha){ //if this bit not belongs to the channel A
                if (bitsDecoded >= offsetBitsHidden){
                    //read LSB of decodedIMGBytes[indByte] 
                    decodedMsgBytes[indBit] = ImgBytes[contByte] & 1;
                    indBit++;
                }
                bitsDecoded++;
            }
            contByte++;
        }
    }else{
        //show decoded in 1 channel
        let indBit=0;
        let contByte =  0;
        let bitsDecoded = 0;
        while (indBit < lengthMsg) {
            if (contByte % 4 == hideByte){ //if this bit belongs to the channel (R,G,B or A)
                if (bitsDecoded >= offsetBitsHidden){
                    //read LSB of decodedIMGBytes[indByte] 
                    decodedMsgBytes[indBit] = ImgBytes[contByte] & 1;
                    indBit++;
                }
                bitsDecoded++;
            }
            contByte++;
        }
    }
    return decodedMsgBytes;
}
