//const co = require('co');
const path = require("path");

const generate = require('node-chartist');
const scriptChartist = path.resolve("./src/utils/chartist_libs/chartist.min.js");
const css3Chartist = path.resolve("./src/utils/chartist_libs/chartist.min.css");
const css3_bar = path.resolve("./src/utils/chartist_libs/css_bar_chartist.css");

exports.makeBarChart = async function makeBarChart(p_width = 500, p_height = 480, ObjectData, filenameHTML) {
    if (!ObjectData) 
      throw new Error(`ObjectData must be have this structure: 
        {data: [Array with data],
          labels: [Array with labels],
          labels_title: String,
          data_title: String
        };
      `);

    if (!filenameHTML)
      throw new Error(`filenameHTML must be passed as argument`);
    const options = {
      width: p_width,
      height: p_height,
      axisX: { title: ObjectData.data_title },
      axisY: { title: ObjectData.labels_title }
    };
  
    const bar = await generate('bar', options, {
      labels: ObjectData.labels,
      series: [
        {name: ObjectData.data_title, value: ObjectData.data}
      ]}, 
    );

    let html = `<html><head><script src="${scriptChartist}"></script>
              <link rel="stylesheet" href="${css3Chartist}"></link>
              <link rel="stylesheet" href="${css3_bar}"></link> 
              </head>
              <body>
              ${bar}
              </body>
              </html>`;
    //save HTML to disk          
    require("fs").writeFileSync(filenameHTML,html);

  // options function
  /*const options = (Chartist) => ({width: 400, height: 200, axisY: { type: Chartist.FixedScaleAxis } });
  const data = {
    labels: ['a','b','c','d','e'],
    series: [
      [1, 2, 3, 4, 5],
      [3, 4, 5, 6, 7]
    ]
  };
  const bar = yield generate('bar', options, data); //=> chart HTML
    */
}

/*
let dataTest = {data: [45,54,82,34,35],
  labels: [0, 1, 2, 3, 4],
  labels_title: "Cantidad",
  data_title: "Byte"
};

makeBarChart(800,480, dataTest);
*/