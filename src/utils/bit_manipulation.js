exports.convertBufferBytetoBufferBit = (bufferByte) => {
    let bufferBit = Buffer.alloc(bufferByte.length*8);
    let indBit = 0;
    for (let index = 0; index < bufferByte.length; index++) {
        const byte = bufferByte[index];
        for (let Bit = 0; Bit < 8; Bit++) {
            bufferBit[indBit++] = byte >> 7 - Bit & 1; 
        }
    }
    return bufferBit;
}

exports.convertBufferBitstoBufferBytes = (bufferBit) => {
    let bufferByte = Buffer.alloc(bufferBit.length/8);
    let indBit = 0;

    for (let index = 0; index < bufferByte.length; index++) {
        for (let Bit = 7; Bit >= 0; Bit--) {
            bufferByte[index] = this.updateBit(bufferByte[index], Bit, bufferBit[indBit++]); 
        }
    }
    return bufferByte;
}


exports.Int32toBytes = (num) => {
    let arr = Buffer.from([
         (num & 0xff000000) >> 24,
         (num & 0x00ff0000) >> 16,
         (num & 0x0000ff00) >> 8,
         (num & 0x000000ff)
    ]);
    return arr;
}

exports.BytestoInt32 = (buffer) => {
    return buffer.readInt32BE();
}

//taken from https://github.com/trekhleb/javascript-algorithms/blob/master/src/algorithms/math/bits/updateBit.js
exports.updateBit = (number, bitPosition, bitValue) => {
    // Normalized bit value.
    const bitValueNormalized = bitValue ? 1 : 0;
    // Init clear mask.
    const clearMask = ~(1 << bitPosition);
    // Clear bit value and then set it up to required value.
    return (number & clearMask) | (bitValueNormalized << bitPosition);
}

exports.getBit = (number, bitPosition) => {
    return (number >> bitPosition) & 1;
}
//end taken