const jimp_lib = require('jimp');

exports.readBytesImage = async (strFileName) => {
    try{
        let obj_jimp = await jimp_lib.read(strFileName); //read the Image file in a JIMP object
        return obj_jimp.bitmap.data; //get the bytes to contain hide
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in readBytesImage, the error is: "${error}"`);
    }

}

exports.LoadBitmapImage = async (strFileName) => {
    try{
        let obj_jimp = await jimp_lib.read(strFileName); //read the Image file in a JIMP object
        return obj_jimp.bitmap; //get the bytes to contain hide
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in readBytesImage, the error is: "${error}"`);
    }
}


exports.writeBytesImage = async (bytes, strFileName, width, height, options) => {
    try{
        let options_received;
        if (!options)
            options_received = {rgba: true,
                                deflateLevel: 1,
                                filterType: jimp_lib.PNG_FILTER_NONE,
                                background: 0xFFFFFFFF
                                };
        else
            options_received = {rgba: options.rgba == undefined ? true: options.rgba,
                                 deflateLevel: options.deflateLevel ? options.deflateLevel: 1,
                                 filterType: options.filterType ? options.filterType : jimp_lib.PNG_FILTER_NONE,
                                 background: options.background? options.background : 0xFFFFFFFF
                                };
        let obj_jimp = new jimp_lib({data: bytes, width, height}, async (err, image) => {
            await image.rgba(options_received.rgba)
            .deflateLevel(options_received.deflateLevel)
            .filterType(options_received.filterType)
            .background(options_received.background)
            .writeAsync(`${strFileName}`); //.${image.getExtension()}
        });        
    }catch(error){
        //TODO: Check the correct way that doesnt appear many error messages on the console
        throw new Error(`Error in writeBytesImage, the error is: "${error}"`);
    }
}
