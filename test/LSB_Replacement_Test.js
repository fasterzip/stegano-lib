const LSB = require("../src/methods_stegano/LSB_Replacement_Image"); //our lib
const path = require("path");

//call the functions test here:
async function test () {
    try{
        //Channel LSB Replacement test
        //input test
        let strImage = path.resolve('./test/img_test/King_bugs_bunny.png');
        let strHidingFile =  path.resolve('./test/img_test/message_hidden.txt');
        let strImageSaved = path.resolve('./test/img_test/bugs_bunny_stegano_channel.png');
        let channel = LSB.ChannelImage.red; 
        let strRecoverFile = path.resolve('./test/img_test/recovered_hidden_CHANNEL.txt');
        //end input test

        await LSB.encodeImg(strImage, strHidingFile, strImageSaved, channel);
        console.log(`Saved in file ${strImageSaved} on channel ${channel}`);
        await LSB.decodeImg(strImageSaved, strRecoverFile, channel)
        console.log(`Recovered in file ${strRecoverFile} on channel ${channel}`);
        //END Channel LSB Replacement test

        //RGB LSB Replacement test
        //input test
        strImage = path.resolve('./test/img_test/King_bugs_bunny.png');
        strHidingFile =  path.resolve('./test/img_test/message_hidden.txt');
        strImageSaved = path.resolve('./test/img_test/bugs_bunny_stegano_rgb.png');
        channel = LSB.ChannelImage.RGB; 
        strRecoverFile = path.resolve('./test/img_test/recovered_hidden_RGB.txt');
        //end input test

        await LSB.encodeImg(strImage, strHidingFile, strImageSaved, channel);
        console.log(`Saved in file ${strImageSaved} on channel RGB`);
        await LSB.decodeImg(strImageSaved, strRecoverFile, channel)
        console.log(`Recovered in file ${strRecoverFile} on channel RGB`);        
        //END RGB LSB Replacement test
        
    }catch(error){
        console.error(`Error in code test, the error is: "${error}"`);
    }
}

test();