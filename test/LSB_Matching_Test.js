const LSB_Matching_image = require("../src/methods_stegano/LSB_Matching_image"); //our lib
const path = require("path");

//call the functions test here:
async function test () {
    try{
        //Channel LSB Replacement test
        //input test
        let strImage = path.resolve('./test/img_test/King_bugs_bunny.png');
        let strHidingFile =  path.resolve('./test/message_hidden.txt');
        let strImageSaved = path.resolve('./test/img_test/bugs_bunny_stegano_channel_matching.png');
        let channel = LSB_Matching_image.ChannelImage.blue; 
        let strRecoverFile = path.resolve('./test/recovered_hidden_CHANNEL.txt');
        //end input test

        await LSB_Matching_image.encodeImg(strImage, strHidingFile, strImageSaved, channel);
        console.log(`Saved in file ${strImageSaved} on channel ${channel}`);
        await LSB_Matching_image.decodeImg(strImageSaved, strRecoverFile, channel)
        console.log(`Recovered in file ${strRecoverFile} on channel ${channel}`);
        //END Channel LSB Replacement test

        //RGB LSB Matching test
        //input test
        strImage = path.resolve('./test/img_test/King_bugs_bunny.png');
        strHidingFile =  path.resolve('./test/message_hidden.txt');
        strImageSaved = path.resolve('./test/img_test/bugs_bunny_stegano_rgb_matching.png');
        channel = LSB_Matching_image.ChannelImage.RGB; 
        strRecoverFile = path.resolve('./test/recovered_hidden_RGB_Matching.txt');
        //end input test

        await LSB_Matching_image.encodeImg(strImage, strHidingFile, strImageSaved, channel);
        console.log(`Saved in file ${strImageSaved} on channel RGB`);
        await LSB_Matching_image.decodeImg(strImageSaved, strRecoverFile, channel)
        console.log(`Recovered in file ${strRecoverFile} on channel RGB`);        
        //END RGB LSB Matching test
        
    }catch(error){
        console.error(`Error in code test, the error is: "${error}"`);
    }
}

test();