const path = require("path");

let strImageFile = path.resolve('./test/0xword_test/ej1_stepic.tif');//'./test/img_test/bugs_bunny_stegano_rgb.png');// //King_bugs_bunny.png
let strResultFile = path.resolve('./test/0xword_test/Visual_attack_ej1_stepic.tif');//'./test/img_test/Visual_attack_bugs_bunny_stegano_rgb.png');  

//Visual Attack Test
const visualAttack = require("../src/steganalysis/visual_attack");

async function visualAttack_test(){
    try {
        await visualAttack.visual_attack_RGB_File(strImageFile, strResultFile);
    } catch (error) {
        throw(error);
    }
}

visualAttack_test().then(() =>{
    console.log("visualAttack_test OK");
}).catch((error)=>{
    console.error(error);
});

//END Visual Attack Test

//Start histogram analysis test
const histogram_analysis = require("../src/steganalysis/histogram_analysis");

async function histogram_analysis_test(){
    try {
        return await histogram_analysis.histogramAnalysisFile(strImageFile); //difference
    } catch (error) {
        throw(error);
    }
}

histogram_analysis_test().then((difference) =>{
    console.log(`Difference between pixels is ${difference}`);
    console.log("Histogram analysis OK");
}).catch((error)=>{
    console.error(error);
});

async function histogram_count_test(){
    try {
        let htmlFileTest = path.resolve("./test/grafica.html");
        //returns array with count of 0...255
        let arrayCountValues = await histogram_analysis.histogramCountFile(strImageFile);
        await histogram_analysis.saveHistogram(arrayCountValues, htmlFileTest); 
    } catch (error) {
        throw(error);
    }
}

histogram_count_test().then(() =>{
    console.log("Histogram count OK");
}).catch((error)=>{
    console.error(error);
});



//end histogram analysis test